# mytools

## Name
     __  __         _____           _     
    |  \/  |_   _  |_   _|__   ___ | |___ 
    | |\/| | | | |   | |/ _ \ / _ \| / __|
    | |  | | |_| |   | | (_) | (_) | \__ \
    |_|  |_|\__, |   |_|\___/ \___/|_|___/
            |___/                 

## Description
This repo is for scripts and programs I've written for social engineering and pentesting. I'm still learning so expect scripts to be added and deleted.

## Support
When I get to the point where I believe this could be a decent tool kit I'll add a contact email for this repo

## Contributing
For right now I would like for this to just be tools I have written, but if you have any ideas on how to make my tools better I would love to hear them. Also will make sure to credit anyone who helps improve my tools.

## License
GPL 3 License

