# MicroPython for D1 mini:
# https://micropython.org/resources/source/micropython-1.19.1.zip
# 
try:
    import usocket as socket
except:
    import socket
import network
import esp
import gc
import os

esp.osdebug(None)
gc.collect()

os.mount(machine.SDCard(), "/sd")

ssid = 'D1_Mini' # default ssid
password = 'password123' # default password

ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid=ssid, password=password, authmode=3)
ap.ifconfig(('192.168.0.4', '255.255.255.0', '192.168.0.1', '8.8.8.8'))

print('Connection successful')
print(ap.ifconfig())

def web_page():
    html = """
    <!DOCTYPE html>
    <html>
        <head>
            <title>GHost Access Point</title>
            <style>
                body{
                    background-color: black; 
                    color: green;
                    text-align: center;
                    font-family: "Lucida Console", monospace;
                    font-size: medium;
                }
            </style>
        </head>
        <body>
            <p><!--- Add your message or information here ---></p>
        </body>
    </html>
    """
    return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

blacklist = []
while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    with open('/sd/log.txt', 'a') as log:
        log.write('Got a connection from %s' % str(addr))
    if addr[0] not in blacklist:
        request = conn.recv(1024)
        response = web_page()
        conn.send(response)
        blacklist.append(addr[0])
    elif addr[0] in blacklist:
        print('IP has been blacklisted')
        conn.close()
    else:
        pass
