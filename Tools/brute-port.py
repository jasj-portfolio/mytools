#!/usr/bin/env python3

# this is for educational purposes I am not 
# responsible for any actions someone else uses this script for...

# Also until I update it this script is untested since I wrote it at work 
# use at your own risk...

import socket
import os

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = input('What Ip are we using: ')
port = input('What port are we connecting to: ')

s.connect((ip, int(port)))

words = input('Path to wordlist: ')

with open(words) as f:
    brut3 = f.readlines()
    for i in brut3:
        f0rc3 = os.system(i)
        if 'connected' or 'Connected' not in f0rc3.stdout.decode('utf-8'):
            pass
        elif 'connected' or 'Connected' in f0rc3.stdout.decode('utf-8'):
            print(f0rc3.stdout.decode('utf-8'))
        else:
            print('Something broke...')
    f.close()
