#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std; 

static const char alphnum[]="0123456789" "abcdefghijklmnopqrstuvwxyz"; 
int strLen=sizeof(alphnum)-1; 
char GenRand(){ 
    return alphnum[rand()%strLen]; 
}

bool CheckWord(char const * filename, string search)
{
    int offset; 
    string line;
    ifstream Myfile;
    Myfile.open (filename);

    if (Myfile.is_open())
    {
        while (!Myfile.eof())
        {
            getline(Myfile,line);
            if ((offset = line.find(search, 0)) != string::npos) 
            {
                Myfile.close();
                return true;
            }
        }
        Myfile.close();
    }
    return false;
}

int main() { 

    int i = 0;

    while (i != 254186855){
        fstream MyFile;
        MyFile.open("list.txt", ofstream::app);
       
        bool x;
        int n,c=0;
        srand(time(0));
        n = 10;
        char C;
        string D;
        for(int z=0; z < n; z++){ 
            C=GenRand();
            D+=C;
            if(isdigit(C)){
                c++;
            }
        }
        x = CheckWord("list.txt", D);
        if ( x != true){
            MyFile<<D<< endl;
            MyFile.close();
            i++;
            cout << i << endl;
        }
    }
    cout << "Done." << endl;
    return 0; 
}
